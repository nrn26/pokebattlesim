import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveControllerTest {

    public static final String[] MOVE_NAMES = new String[]{"Thunder", "Quick Attack", "Thunderbolt", "Stomp"};
    public static final String[] MOVE_NAMES2 = new String[]{"Hydro Pump", "Surf", "Headbutt", "Slam"};
    public static final String[] FIRST_MOVE_STATS = new String[]{"Thunder", "Electric", "110", "75", "special"};
    List<Move> moves;
    Move firstMove;

    @BeforeEach
    void setUp() {
        moves = MoveController.generateMoveSet(MOVE_NAMES);
        firstMove = moves.get(0);
    }

    @Test
    void move_controller_generates_four_moves() {
        assertEquals(4, moves.size());
    }

    @Test
    void move_controller_creates_two_move_sets() {
        List<Move> moves2 = MoveController.generateMoveSet(MOVE_NAMES2);

        assertEquals(MOVE_NAMES2[0], moves2.get(0).getName());
    }

    @Test
    void move_controller_creates_correct_move_name() {
        assertEquals(firstMove.getName(), FIRST_MOVE_STATS[0]);
    }

    @Test
    void move_controller_create_correct_move_elemental_type() {
        assertEquals(firstMove.getType(), FIRST_MOVE_STATS[1]);
    }

    @Test
    void move_controller_create_correct_move_power() {
        assertEquals(firstMove.getPower(), Integer.parseInt(FIRST_MOVE_STATS[2]));
    }

    @Test
    void move_controller_create_correct_move_accuracy() {
        assertEquals(firstMove.getAccuracy(), Integer.parseInt(FIRST_MOVE_STATS[3]));
    }

    @Test
    void move_controller_create_correct_move_type() {
        assertEquals(firstMove.getMoveType(), FIRST_MOVE_STATS[4]);
    }

}
