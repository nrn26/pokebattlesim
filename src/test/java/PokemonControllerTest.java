import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PokemonControllerTest {

    public static final String INPUT_STRING = "Pikachu,Electric,80,90,55,90,80,110,Thunder,Quick Attack,Thunderbolt,Stomp";
    public static final String NAME = "Pikachu";
    public static final List<String> INPUT_TOKENS = new ArrayList<>(Arrays.asList(INPUT_STRING.split(",")));
    PokemonController controller;

    @BeforeEach
    void setUp() {
        controller = new PokemonController();
    }

    @Test
    void controller_can_create_a_pokemon_from_an_input_string() {
        Pokemon pokemon = controller.createPokemon(INPUT_TOKENS);

        assertEquals(NAME, pokemon.getName());
    }
}
