import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.print.attribute.standard.MediaSize;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class MoveTest {

    public static final String NAME = "Thunder Shock";
    public static final String TYPE = "Electric";
    public static final int POWER = 50;
    public static final int ACCURACY = 100;
    public static final String MOVETYPE = "special";
    Move move;

    @BeforeEach
    void setUp() {
        move = new Move(NAME, TYPE, POWER, ACCURACY, MOVETYPE);
    }

    @Test
    void move_has_name() {
        assertEquals(NAME, move.getName());
    }

    @Test
    void move_has_type() {
        assertEquals(TYPE, move.getType());
    }

    @Test
    void move_has_power() {
        assertEquals(POWER, move.getPower());
    }

    @Test
    void move_has_accuracy() {
        assertEquals(ACCURACY, move.getAccuracy());
    }

    @Test
    void move_has_move_type() {
        assertEquals(MOVETYPE, move.getMoveType());
    }

    @Test
    void move_can_miss() {
        boolean missed = move.missed();
        assertFalse(missed);
    }


}
