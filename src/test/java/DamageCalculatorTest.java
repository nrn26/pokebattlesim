import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DamageCalculatorTest {

    public static final int BASE_DAMAGE = 14;
    Pokedex pokedex;
    Pokemon attackingPokemon;
    Pokemon defendingPokemon;
    Move move;

    @BeforeEach
    void setUp() {
        pokedex = new Pokedex();
        attackingPokemon = pokedex.getPokemon("Pikachu");
        defendingPokemon = pokedex.getPokemon("Starmie");
        move = attackingPokemon.pickRandomMove();
    }

    @Test
    void calculator_takes_two_pokemon_and_a_move() {
       int damageDealt = DamageCalculator.calculate(attackingPokemon, move, defendingPokemon);

       assertTrue(damageDealt > 0);
    }

    @Test
    void calculator_returns_damage_based_off_level_30_which_is_same_for_both_pokemon_and_can_be_adjusted_behind_the_scenes() {
        int damageDealt = DamageCalculator.calculate(attackingPokemon, move, defendingPokemon);

        assertTrue(damageDealt >= BASE_DAMAGE);
    }

    @Test
    void calculator_takes_move_power_into_account_as_well() {
        int damageDealt = DamageCalculator.calculate(attackingPokemon, move, defendingPokemon);

        int movePower = move.getPower();

        assertTrue(BASE_DAMAGE * movePower >= damageDealt);
    }

    @Test
    void calculator_gets_numerator_and_denominator_calculated_correctly() {
        int damageDealt = DamageCalculator.calculate(attackingPokemon, move, defendingPokemon);
        int movePower = move.getPower();

        assertTrue(damageDealt > 0);
    }



}
