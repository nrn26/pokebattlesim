import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PokemonTest {

    public static final String POKEMON_NAME = "Pikachu";
    public static final String POKEMON_TYPE = "Electric";
    public static final int[] POKEMON_STATS = {80, 90, 55, 90, 80, 110};
    public static final String POKEMON2_NAME = "Squirtle";
    public static final String POKEMON2_TYPE = "Water";
    public static final int[] POKEMON2_STATS = {60, 80, 50, 75, 85, 100};
    Pokemon pokemon;
    Pokemon pokemon2;
    Move move;
    Move move2;

    @BeforeEach
    void setUp() {
        pokemon = new Pokemon(POKEMON_NAME, POKEMON_TYPE, POKEMON_STATS);
        pokemon2 = new Pokemon(POKEMON2_NAME, POKEMON2_TYPE, POKEMON2_STATS);

        move = new Move("Thunder Shock", "Electric", 50, 100, "special");
        move2 = new Move("Thunderbolt", "Electric", 90, 100, "special");
    }

    @Test
    void pokemon_has_name() {
        assertEquals(POKEMON_NAME, pokemon.getName());
        assertEquals(POKEMON2_NAME, pokemon2.getName());
    }

    @Test
    void pokemon_has_moveset() {
        assertTrue(pokemon.getMoves().isEmpty());
        assertTrue(pokemon2.getMoves().isEmpty());
    }

    @Test
    void pokemon_has_stats() {
        assertEquals(POKEMON_STATS[0], pokemon.getStat("HP"));
        assertEquals(POKEMON_STATS[1], pokemon.getStat("Attack"));
        assertEquals(POKEMON_STATS[2], pokemon.getStat("Defense"));
        assertEquals(POKEMON_STATS[3], pokemon.getStat("Special Attack"));
        assertEquals(POKEMON_STATS[4], pokemon.getStat("Special Defense"));
        assertEquals(POKEMON_STATS[5], pokemon.getStat("Speed"));
    }

    @Test
    void pokemon_has_type() {
        assertEquals(POKEMON_TYPE, pokemon.getType());
        assertEquals(POKEMON2_TYPE, pokemon2.getType());
    }

    @Test
    void pokemon_uses_first_move() {
        pokemon.learn(move);
        int damage = pokemon.use(1);
        assertEquals(move.getPower(), damage);
    }

    @Test
    void pokemon_uses_second_move() {
        pokemon.learn(move);
        pokemon.learn(move2);
        int damage = pokemon.use(2);

        assertEquals(move2.getPower(), damage);
    }

    @Test
    void pokemon_cannot_learn_same_move_twice() {
        pokemon.learn(move);
        int beforeLength = pokemon.getMoves().size();
        pokemon.learn(move);
        int afterLength = pokemon.getMoves().size();

        assertEquals(beforeLength, afterLength);
    }

    @Test
    void pokemon_attacks_another_pokemon() {
        pokemon.learn(move);
        int previousHP = pokemon2.getStat("HP");
        pokemon.attack(pokemon2, 1);
        int currentHP = pokemon2.getStat("HP");

        assertTrue(currentHP < previousHP);
    }

    @Test
    void pokemon_hp_cannot_go_below_zero() {
        pokemon.learn(move);
        pokemon.attack(pokemon2, 1);
        pokemon.attack(pokemon2, 1);

        assertEquals(0, pokemon2.getStat("HP"));
    }


}
