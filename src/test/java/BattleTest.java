import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BattleTest {

    @Test
    void battle_with_no_parameters_selects_two_random_pokemon() {
        Battle battle = new Battle();

        assertNotNull(battle.getPokemon(1));
        assertNotNull(battle.getPokemon(2));
    }

    @Test
    void battle_with_one_parameter_picks_pokemon_by_id_and_other_pokemon_is_random() {
        Battle battle = new Battle(2);

        assertEquals("Charizard", battle.getPokemon(1).getName());
        assertNotNull(battle.getPokemon(2));
    }

    @Test
    void battle_can_take_two_id_parameters_to_pick_both_pokemon_who_will_fight_in_battle() {
        Battle battle = new Battle(41, 66);

        assertEquals("Onix", battle.getPokemon(1).getName());
        assertEquals("Gyarados", battle.getPokemon(2).getName());
    }

    @Test
    void battle_can_take_place_between_same_pokemon() {
        Battle battle = new Battle(1, 1);

        assertEquals(battle.getPokemon(1), battle.getPokemon(2));
    }

    @Test
    void battle_can_take_a_name_as_parameter_and_other_pokemon_is_random() {
        Battle battle = new Battle("Pikachu");

        assertEquals("Pikachu", battle.getPokemon(1).getName());
        assertNotNull(battle.getPokemon(2));
    }

    @Test
    void battle_can_take_two_names_as_parameters() {
        Battle battle = new Battle("Starmie", "Weezing");

        assertEquals("Starmie", battle.getPokemon(1).getName());
        assertEquals("Weezing", battle.getPokemon(2).getName());
    }

    @Test
    void random_selection_for_which_pokemon_goes_first() {
        Battle battle = new Battle("Pikachu", "Blastoise");

        assertTrue(battle.getCurrentTurnNumber() == 1 || battle.getCurrentTurnNumber() == 2);
    }

    @Test
    void battle_can_check_if_either_pokemon_has_fainted_returns_false_if_not() {
        Battle battle = new Battle();

        assertFalse(battle.checkIfEitherFainted());
    }

    @Test
    void battle_can_confirm_a_pokemon_has_fainted_once_hp_reaches_zero() {
        Battle battle = new Battle();

        Pokemon firstPokemon = battle.getPokemon(1);
        int originalHP = firstPokemon.getStat("HP");
        firstPokemon.takeDamage(originalHP);

        assertTrue(battle.checkIfEitherFainted());
    }

    @Test
    void battle_can_get_move_number_used_for_current_turn() {
        Battle battle = new Battle();
        int moveNumber = battle.getCurrentMoveNumber();

        assertTrue(0 <= moveNumber && moveNumber < 4);
    }

    //@Test
    void battle_can_execute_attacking_pokemons_move() {
        Battle battle = new Battle();
        int moveNumber = battle.getCurrentMoveNumber();
        //battle.pokemonUsesMove(moveNumber);
    }


}
