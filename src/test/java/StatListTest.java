import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StatListTest {

    public static final int[] STATS = {80, 90, 55, 90, 80, 110};
    StatList stats;

    @BeforeEach
    void setUp() {
        stats = new StatList(STATS);
    }

    @Test
    void stat_list_has_hp() {
        assertEquals(STATS[0], stats.get("hp"));
    }

    @Test
    void stat_list_has_attack() {
        assertEquals(STATS[1], stats.get("attack"));
    }

    @Test
    void stat_list_has_defense() {
        assertEquals(STATS[2], stats.get("defense"));
    }

    @Test
    void stat_list_has_special_attack() {
        assertEquals(STATS[3], stats.get("special attack"));
    }

    @Test
    void stat_list_has_special_defense() {
        assertEquals(STATS[4], stats.get("special defense"));
    }

    @Test
    void stat_list_has_speed() {
        assertEquals(STATS[5], stats.get("speed"));
    }


}
