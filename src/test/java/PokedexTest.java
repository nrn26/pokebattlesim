import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class PokedexTest {

    public static final int NUMBER_OF_POKEMON = 82;
    Pokedex pokedex;

    @BeforeEach
    void setUp() {
        pokedex = new Pokedex();
    }

    @Test
    void pokedex_has_all_82_final_evolution_pokemon() {
        assertEquals(NUMBER_OF_POKEMON, pokedex.size());
    }

    @Test

    void first_pokemon_is_venasaur() {
        assertEquals("Venasaur", pokedex.getPokemon(1).getName());
    }

    @Test
    void last_pokemon_is_mew() {
        assertEquals("Mewtwo", pokedex.getPokemon(81).getName());
    }
    @Test
    void can_get_any_pokemon_by_ID() {
        assertEquals("Arcanine", pokedex.getPokemon(25).getName());
    }

    @Test
    void can_get_any_pokemon_by_name() {
        assertEquals("Electabuzz", pokedex.getPokemon("Electabuzz").getName());
    }

    @Test
    void pokedex_returns_null_if_cannot_find_pokemon_by_ID() {
        assertNull(pokedex.getPokemon(0));
    }

    @Test
    void pokedex_returns_null_if_cannot_find_pokemon_by_name() {
        assertNull(pokedex.getPokemon("Garchomp"));
    }
}