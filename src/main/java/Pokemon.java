import java.util.ArrayList;
import java.util.List;

public class Pokemon {
    private final String name;
    private final String type;
    private final List<Move> moveset;
    private StatList stats;

    public Pokemon(String name, String type, int[] statValues) {
        this.name = name;
        this.type = type;
        this.moveset = new ArrayList<>();
        this.stats = new StatList(statValues);
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public List<Move> getMoves() {
        return moveset;
    }

    public int getStat(String stat) {
        stat = stat.toLowerCase();
        return stats.get(stat);
    }

    public int use(int moveNumber) {
        Move move = moveset.get(moveNumber-1);
        if (move.missed()) {
            return 0;
        } else {
            return move.getPower();
        }
    }

    public void learn(Move move) {
        if (isNewMove(move)) {
            moveset.add(move);
        }
    }

    public void attack(Pokemon pokemon2, int moveNumber) {
        pokemon2.takeDamage(this.use(moveNumber));
    }

    private boolean isNewMove(Move move) {
        for (Move m : moveset) {
            if (move.getName().equals(m.getName())) {
                return false;
            }
        }
        return true;
    }

    protected void takeDamage(int damageTaken) {
        stats.put("hp", Math.max(stats.get("hp") - damageTaken, 0));
    }

    protected Move pickRandomMove() {
        return moveset.get(pickRandomMoveNumber());
    }

    protected int pickRandomMoveNumber() {
        return (int) Math.floor(Math.random() * 4);
    }
}
