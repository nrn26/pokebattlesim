public class Battle {
    private Pokedex pokedex = new Pokedex();
    private Pokemon firstPokemon;
    private Pokemon secondPokemon;
    private int currentTurnNumber = (int) (Math.round(Math.random())+1);

    public Battle() {
        this.firstPokemon = pokedex.getPokemon(randomPokemon());
        this.secondPokemon = pokedex.getPokemon(randomPokemon());
    }

    public Battle(int firstPokemonID) {
        this.firstPokemon = pokedex.getPokemon(firstPokemonID);
        this.secondPokemon = pokedex.getPokemon(randomPokemon());
    }

    public Battle(int firstPokemonID, int secondPokemonID) {
        this.firstPokemon = pokedex.getPokemon(firstPokemonID);
        this.secondPokemon = pokedex.getPokemon(secondPokemonID);
    }

    public Battle(String firstPokemonName) {
        this.firstPokemon = pokedex.getPokemon(firstPokemonName);
        this.secondPokemon = pokedex.getPokemon(randomPokemon());
    }

    public Battle(String firstPokemonName, String secondPokemonName) {
        this.firstPokemon = pokedex.getPokemon(firstPokemonName);
        this.secondPokemon = pokedex.getPokemon(secondPokemonName);
    }


    public Pokemon getPokemon(int playerNumber) {
        return playerNumber == 1 ? this.firstPokemon : this.secondPokemon;
    }

    private int randomPokemon() {
        return (int) Math.floor(Math.random()*(82 - 1 +1)+ 1);
    }

    public int getCurrentTurnNumber() {
        return this.currentTurnNumber;
    }

    public boolean checkIfEitherFainted() {
        return (this.firstPokemon.getStat("HP") == 0) ||
                (this.secondPokemon.getStat("HP") == 0);
    }

    public int getCurrentMoveNumber() {
        Pokemon currentPokemon = getPokemon(getCurrentTurnNumber());
        return currentPokemon.pickRandomMoveNumber();
    }

    /*public void pokemonUsesMove(int moveNumber) {
        Pokemon attackingPokemon = getPokemon(getCurrentTurnNumber());
        Pokemon defendingPokemon = (getCurrentTurnNumber() == 1) ? getPokemon(2) : getPokemon(1);
        Move move = attackingPokemon.getMoves().get(getCurrentMoveNumber());

        //DamageCalculator.calculate(attackingPokemon, move, defendingPokemon);
    }*/
}
