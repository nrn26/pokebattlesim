public class DamageCalculator {
    private static final int level = 30;

    public static int calculate(Pokemon attacker, Move move, Pokemon defender) {
        double attackDefenseRatio = getAttackDefenseRatio(attacker, move, defender);

        return (int) calculateNumerator(move, attackDefenseRatio) / 50 + 2;
    }

    private static double calculateNumerator(Move move, double attackDefenseRatio) {
        return levelDamage() * move.getPower() * attackDefenseRatio;
    }

    private static double getAttackDefenseRatio(Pokemon attacker, Move move, Pokemon defender) {
        int attackDefenseRatio;
        if (move.getMoveType().equals("physical")) {
            attackDefenseRatio = attacker.getStat("attack") / defender.getStat("defense");
        } else {
            attackDefenseRatio = attacker.getStat("special attack") / defender.getStat("special defense");
        }
        return attackDefenseRatio;
    }

    private static int levelDamage() {
        return (2 * level / 5) + 2;
    }
}
