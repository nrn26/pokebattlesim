import java.util.HashMap;

public class Pokedex {
    private HashMap<Integer, Pokemon> pokedex = new HashMap<>();
    private PokemonController controller;


    public Pokedex() {
        this.controller = new PokemonController();
        this.controller.initializeAllPokemon(pokedex);
    }

    public int size() {
        return pokedex.size();
    }

    public Pokemon getPokemon(Integer ID) {
        return pokedex.get(ID);
    }

    public Pokemon getPokemon(String name) {
        return pokedex.values().stream().filter(pokemon -> pokemon.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }
}
