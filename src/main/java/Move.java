public class Move {
    private final String name;
    private final String type;
    private final int power;
    private final int accuracy;
    private final String moveType;

    Move(String name, String type, int power, int accuracy, String moveType) {
        this.name = name;
        this.type = type;
        this.power = power;
        this.accuracy = accuracy;
        this.moveType = moveType;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public int getPower() {
        return power;
    }

    public boolean missed() {
        int randomInt = (int)(Math.random() * (100 - 1 + 1) + 1);
        return randomInt <= 100 - accuracy;
    }

    public String getMoveType() {
        return moveType;
    }
}
