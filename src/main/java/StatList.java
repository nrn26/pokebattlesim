import java.util.HashMap;
import java.util.Map;

public class StatList {
    private final Map<String, Integer> stats;

    StatList(int[] rawStats) {
        stats = new HashMap<>();
        initializeHashMap(rawStats);
    }

    int get(String stat) {
        return stats.get(stat);
    }

    void put(String stat, int amount) {
        stats.put(stat, amount);
    }

    private void initializeHashMap(int[] rawStats) {
        stats.put("hp", rawStats[0]);
        stats.put("attack", rawStats[1]);
        stats.put("defense", rawStats[2]);
        stats.put("special attack", rawStats[3]);
        stats.put("special defense", rawStats[4]);
        stats.put("speed", rawStats[5]);
    }
}
