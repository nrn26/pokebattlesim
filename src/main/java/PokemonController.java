import java.util.*;

public class PokemonController {
    private static final String POKE_FILE = "./src/main/resources/pokemon.csv";

    public void initializeAllPokemon(HashMap<Integer, Pokemon> pokedex) {
        List<List<String>> pokemonData = CSVReader.readFile(POKE_FILE);
        int ID = 1;
        for (List<String> lineTokens : pokemonData) {
            Pokemon pokemon = createPokemon(lineTokens);
            pokedex.put(ID, pokemon);
            ID += 1;
        }
    }

     protected Pokemon createPokemon(List<String> tokens) {
        String name = tokens.get(0);
        String type = tokens.get(1);
        int[] stats = getStats(tokens);
        String[] moveNames = new String[4];
        for (int i = 8; i < tokens.size(); i++) {
            moveNames[i-8] = tokens.get(i);
        }

        return create(name, type, stats, moveNames);
    }

    private Pokemon create(String name, String type, int[] stats, String[] moveNames) {
        Pokemon pokemon = new Pokemon(name, type, stats);
        initializePokemonMoveSet(pokemon, moveNames);
        return pokemon;
    }

    private int[] getStats(List<String> tokens) {
        return new int[]{Integer.parseInt(tokens.get(2)), Integer.parseInt(tokens.get(3)),
                Integer.parseInt(tokens.get(4)), Integer.parseInt(tokens.get(5)),
                Integer.parseInt(tokens.get(6)), Integer.parseInt(tokens.get(7))};
    }

    private void initializePokemonMoveSet(Pokemon pokemon, String[] moveNames) {
        List<Move> moveSet = MoveController.generateMoveSet(moveNames);
        for (int i = 0; i < moveNames.length; i++) {
            pokemon.learn(moveSet.get(i));
        }
    }
}
