import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class MoveController {
    private static final String MOVE_FILE = "./src/main/resources/moves.csv";

    private MoveController() {
        throw new IllegalStateException("Utility class");
    }

    static List<Move> generateMoveSet(String[] moveNames) {
        List<List<String>> moveData = CSVReader.readFile(MOVE_FILE);
        return createMoveSet(moveNames, moveData);
    }

    private static List<Move> createMoveSet(String[] moveNames, List<List<String>> moveData) {
        List<Move> moveSet = new ArrayList<>();
        for (String moveName : moveNames) {
            List<String> moveTokens = parseMoveLine(moveName, moveData);
            moveSet.add(createMove(moveTokens));
        }
        return moveSet;
    }

    private static Move createMove(List<String> moveTokens) {
        return new Move(moveTokens.get(0), moveTokens.get(1), Integer.parseInt(moveTokens.get(2)), Integer.parseInt(moveTokens.get(3)), moveTokens.get(4));
    }

    private static List<String> parseMoveLine(String targetMoveName, List<List<String>> moveData) {
        for (List<String> lineTokens : moveData) {
            if (lineTokens.get(0).equals(targetMoveName)) {
                return lineTokens;
            }
        }
        return Collections.emptyList();
    }

}
